python server steps:

Details:

- Flask Server w/ CRUD operations

Development:

- Clone Repo
 ```
 git init
 git add .
 git commit
 git push
 git clone https...
 ```

- Start up virtual environment
```
python -m venv <"custom name">
 - storing inside a virtual folder so you don't pollute global computer dep
 - api-server is the folders name
 - what does this do?
```

- Source Virtual Env
```
> cd into <"custom name">

source bin/activate
> when you create env. than you are activating the binary file
> deactivate to get out of venv
. source bin/activate to get into venv
```

- Install dependencies
```
> pip install Flask
> pip freeze > requirements.txt

*pip is a package manager. pip comes with python like npm comes with nodejs
*like a ableton plugin/library
```

- Start Flask Sever
```
python main.py
```
