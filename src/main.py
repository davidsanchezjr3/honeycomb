from bourbon import bourbon
from flask import Flask

app = Flask(__name__)


@app.route("/health", methods=["GET"])
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/bourbon_list", methods=["GET"])
def bourbon_list():
    return bourbon.get_bourbon_list("test")


@app.route("/dogs", methods=["GET"])
def root_request():
    return "<p>API FOUND</p>"


@app.route("/get_date")
def get_date():
    return "1"


# CRUD
# Get, Post, Put, Delete
@app.route("/create_item", methods=["POST"])
def create_item(req):
    print(req)
    return "success!"


app.run()
